package com.example.tetsapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.tetsapplication.model.Post;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private PostAPI postAPI;
    private Button btnBack;
    private Button btnNext;
    private TextView tvDescription;
    private ImageView ivGif;
    private ProgressBar pbGif;
    private LinearLayout errorMessage;
    private Button btnUpdate;
    private List<Post> posts;

    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createPostAPI();

        position = -1;
        posts = new LinkedList<>();


        ivGif = (ImageView) findViewById(R.id.iv_gif);
        btnBack = (Button) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previousPost();
            }
        });
        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextPost();
            }
        });
        tvDescription = (TextView) findViewById(R.id.tv_description);
        pbGif = (ProgressBar) findViewById(R.id.pb_gif);
        errorMessage = (LinearLayout) findViewById(R.id.error_message);
        errorMessage.setVisibility(View.GONE);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                errorMessage.setVisibility(View.GONE);
                btnBack.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.VISIBLE);
                ivGif.setVisibility(View.VISIBLE);
                tvDescription.setVisibility(View.VISIBLE);
                nextPost();
            }
        });

        nextPost();

    }

    private void nextPost() {
        if (posts.size() == position+1) {
            System.out.println(position);
            Call<Post> call = postAPI.getPost();
            call.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {
                    if (response.isSuccessful()) {
                        Post post = response.body();
                        posts.add(post);
                        position++;
                        changePostView();
                    }
                    else {
                        errorMessage.setVisibility(View.VISIBLE);
                        btnBack.setVisibility(View.GONE);
                        btnNext.setVisibility(View.GONE);
                        ivGif.setVisibility(View.GONE);
                        tvDescription.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                    errorMessage.setVisibility(View.VISIBLE);
                    btnBack.setVisibility(View.GONE);
                    btnNext.setVisibility(View.GONE);
                    ivGif.setVisibility(View.GONE);
                    tvDescription.setVisibility(View.GONE);
                }
            });
        }
        else {
            position++;
            changePostView();
        }
    }

    private void previousPost() {
        position--;
        changePostView();
    }
    private void changePostView() {
        Post post = posts.get(position);
        pbGif.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(post.getGifURL())
                .asGif()
//                .error(errorMessage.getDividerDrawable())
                .error(R.drawable.ic_broken_image)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .listener(new RequestListener<String, GifDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GifDrawable> target, boolean isFirstResource) {
                        pbGif.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, String model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        pbGif.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(ivGif);


        tvDescription.setText(post.getDescription());
        if (position == 0) {
            btnBack.setEnabled(false);
        }
        else {
            btnBack.setEnabled(true);
        }
    }


    private void createPostAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PostAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        postAPI = retrofit.create(PostAPI.class);
    }


}