package com.example.tetsapplication;

import com.example.tetsapplication.model.Post;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostAPI {

    String BASE_URL = "https://developerslife.ru/";

    @GET("random?json=true")
    Call<Post> getPost();
}
